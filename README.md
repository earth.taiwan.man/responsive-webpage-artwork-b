### $`\textcolor{GoldenRod}{\text{響應式網頁的初始化}}`$
```
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="new_bootstrap/dist/css/bootstrap.min.css">
  <title>新商業版型 B</title>

  <style>
    .row .card-title
    {
      background-color: #dff0d8;
      color: #3c763d;
      padding: 10px;
    }

    .card img
    {
      height: 160px;
      width: 95%;
      margin: auto;
    }
  </style>
</head>
```
由上可看出，此網頁的編碼方式 (utf-8)、預設尺寸 (initial-scale=1)、所套用的串接樣式表檔案 (new_bootstrap/dist/css/bootstrap.min.css)，以及卡片元件的相關外觀設定 (「.card」開頭之相關選取器語法)。

### $`\textcolor{GoldenRod}{\text{網頁中之頁首的相關語法}}`$
```
<div class="card" style="background-color: LightGray; text-align: center">
  <div class="card-body">
    <h1 class="card-title">優質旅遊網</h1>
    <h6 class="card-subtitle mb-2 text-muted">真誠、貼心與歡樂</h6>
    <br>
    <div class="col-md-4 mx-auto">
      <form class="input-group mb-3">
        <span class="input-group-text" id="inputGroup-sizing-default">站內搜尋</span>
        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="請輸入關鍵字 ..." >
        <button class="btn btn-success" style="display: inline">搜尋</button>
      </form>
    </div>
  </div>
</div>
```
由上可看出，頁首是由卡片元件所搭建出來的，並且內含頁面標題、副標題，以及搜尋欄位。

### $`\textcolor{GoldenRod}{\text{網頁中之導覽列的相關語法}}`$
```
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="images/賞美行程/earth-logo.png" width="30" style="position: relative ; top: -5px">
    </a>
     ⋮
  </div>
</nav>
```

### $`\textcolor{GoldenRod}{\text{寬度均分之 3 欄式區塊的相關語法}}`$
```
<div class="container">
  <div class="row">
    <div class="col-sm-4">
     ⋮
    </div>
    <div class="col-sm-4">
     ⋮
    </div>
    <div class="col-sm-4">
     ⋮
    </div>

    <p style="height: 5px"></p>
    <div class="col-sm-4">
     ⋮
    </div>
    <div class="col-sm-4">
     ⋮
    </div>
    <div class="col-sm-4">
     ⋮
    </div>
  </div>
</div>
```

### $`\textcolor{GoldenRod}{\text{頁尾的相關語法}}`$
```
<footer class="container-fluid text-center">
  <p>優質旅遊股份有限公司 &copy; 版權所有</p>
</footer>
```

### $`\textcolor{GoldenRod}{\text{繫結 Bootstrap 響應式機制所需的 JavaScript 程式碼檔案 bootstrap.bundle.min.js。}}`$
```
<script src="new_bootstrap/dist/js/bootstrap.bundle.min.js"></script>
```

### $`\textcolor{GoldenRod}{\text{該作品的參考網址 (內含的影像檔較多，懇請包容其載入速度)}}`$

[響應式網頁 之 簡易作品 B](https://francesco-artworks.web.app/responsive/layout-b-new.html)
